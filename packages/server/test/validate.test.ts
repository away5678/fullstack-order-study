import {
  validate,
  validateOrReject,
  Contains,
  IsInt,
  Length,
  IsEmail,
  IsFQDN,
  IsDate,
  Min,
  Max,
} from 'class-validator';
import { IsDateString } from '../src/common/mvc/validator/decorators/is-date-string.decorator';

export class Post {
  @Length(10, 20)
  title: string;

  @Contains('hello')
  text: string;

  @IsInt()
  @Min(0)
  @Max(10)
  rating: number;

  @IsEmail()
  email: string;

  @IsFQDN()
  site: string;

  @IsDate()
  createDate: Date;
}

export class Post1 {
  @IsDateString()
  createDate: string;
}



let post = new Post1();
post.createDate = "2020/03/120"; // should not pass

validate(post).then(errors => {
  // errors is an array of validation errors
  if (errors.length > 0) {
    console.log('validation failed. errors: ', errors);
  } else {
    console.log('validation succeed');
  }
});

// validateOrReject(post).catch(errors => {
//   console.log('Promise rejected (validation failed). Errors: ', errors);
// });
// // or
// async function validateOrRejectExample(input) {
//   try {
//     await validateOrReject(input);
//   } catch (errors) {
//     console.log('Caught promise rejection (validation failed). Errors: ', errors);
//   }
// }
