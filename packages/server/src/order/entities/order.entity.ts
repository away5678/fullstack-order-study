import { BeforeInsert, Column, Entity, PrimaryColumn } from 'typeorm';
import { nanoid } from 'nanoid';


export enum OrderStatus {
  Pending = 'Pending',
  Processing = 'Processing',
  Completed = 'Completed',
}

@Entity({ name: 'a_order' })
export class Order {
  @BeforeInsert()
  private _beforeInsert() {
    if (!this.id)
      this.id = `OD_${nanoid()}`;
  }

  @PrimaryColumn('char', {
    length: 24,
  })
  id: string;

  @Column({
    type: 'varchar',
    length: 100,
    comment: '用户姓名',
  })
  username: string;

  @Column({
    type: 'enum',
    enum: OrderStatus,
    default: OrderStatus.Pending,
    comment: '状态',
  })
  status: OrderStatus;

  @Column({
    type: 'date',
    comment: '订单日期',
  })
  orderDate: Date;
}
