import { IsEnum } from 'class-validator';
import { OrderStatus } from '../entities/order.entity';


export class OrderUpdateDto {

  @IsEnum(OrderStatus, {
    message: '必须是枚举',
  })
  status: OrderStatus;
}
