import { IsEnum, IsNotEmpty, IsOptional } from 'class-validator';
import { IsDateString } from '../../common/mvc/validator/decorators/is-date-string.decorator';
import { OrderStatus } from '../entities/order.entity';


export class OrderCreateDto {
  @IsNotEmpty()
  username: string;

  @IsDateString()
  orderDate: string;
}
