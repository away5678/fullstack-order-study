import { OrderStatus } from '../entities/order.entity';
import { PaginationInputData } from '../../common/mvc/objects/input';
import { IsEnum, IsOptional } from 'class-validator';
import { IsDateString } from '../../common/mvc/validator/decorators/is-date-string.decorator';
import { Transform } from 'class-transformer';

export class OrderSearchDto extends PaginationInputData {

  @Transform((params) => (params.value === '' ? null : params.value))
  @IsOptional()
  username: string;

  @Transform((params) => (params.value === '' ? null : params.value))
  @IsDateString()
  @IsOptional()
  orderDate: string;

  @Transform((params) => (params.value === '' ? null : params.value))
  @IsEnum(OrderStatus, {
    message: '必须是枚举',
  })
  @IsOptional()
  status: OrderStatus;
}
