import { Body, Controller, Delete, Get, Inject, Param, Post, Put, Query } from '@nestjs/common';
import { Order } from '../entities/order.entity';
import { OrderSearchDto } from '../dtos/order-search.dto';
import { OrderService } from '../services/order.service';
import { PaginationOutputData } from '../../common/mvc/objects/result';
import { OrderCreateDto } from '../dtos/order-create.dto';
import { OrderUpdateDto } from '../dtos/order-update.dto';

@Controller('/order')
export class OrderController {

  @Inject()
  private readonly orderService: OrderService;

  @Get('list')
  async list(@Query() dto: OrderSearchDto) {
    return await this.orderService.page(dto) as PaginationOutputData<Order>;
  }

  @Post('create')
  async create(@Body("order") dto: OrderCreateDto) {
    return await this.orderService.create(dto);
  }

  @Put('update/:id')
  async update(@Param('id') id: string, @Body("order") dto: OrderUpdateDto) {
    return await this.orderService.update(id, dto);
  }

  @Delete('delete/:id')
  async delete(@Param('id') id: string) {
    return await this.orderService.delete(id);
  }


}
