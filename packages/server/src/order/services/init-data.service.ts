import { Injectable, Logger, OnModuleInit } from '@nestjs/common';
import { InjectEntityManager } from '@nestjs/typeorm';
import { EntityManager } from 'typeorm';
import { Order, OrderStatus } from '../entities/order.entity';
import { getName } from 'random_chinese_fantasy_names';
import dayjs from '../../common/utils/dayjs.utils';

@Injectable()
export class InitDataService implements OnModuleInit {

  private readonly log = new Logger();

  @InjectEntityManager()
  private manager: EntityManager;

  private names: string[];

  onModuleInit(): any {
    this.log.log('初始化数据...');
    this.names = getName(20);
    this.initOrderData().then();
  }


  private getRandomUsername(): string {
    const randomIndex = Math.floor(Math.random() * this.names.length);
    return this.names[randomIndex];
  }

  private getRandomStatus(): OrderStatus {
    const values: string[] = Object.values(OrderStatus) as string[];
    const randomIndex = Math.floor(Math.random() * values.length);
    return OrderStatus[values[randomIndex]];
  }

  private async initOrderData() {

    await this.manager.transaction(async (tm) => {
      const orderRepo = tm.getRepository(Order);

      if (await orderRepo.count() === 0) {

        for (let i = 0; i < 500; i++) {
          const dateOffset = Math.floor(Math.random() * 300);

          const o = orderRepo.create();

          o.status = this.getRandomStatus();
          o.username = this.getRandomUsername();
          o.orderDate = dayjs().subtract(dateOffset, 'day').toDate();

          await orderRepo.save(o);
        }
      }

    });


  }

}
