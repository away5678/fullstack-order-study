import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Order, OrderStatus } from '../entities/order.entity';
import { Equal, Like, Repository } from 'typeorm';
import { OrderSearchDto } from '../dtos/order-search.dto';
import { OrderCreateDto } from '../dtos/order-create.dto';
import { dayjs } from '../../common/utils/dayjs.utils';
import { OrderUpdateDto } from '../dtos/order-update.dto';

@Injectable()
export class OrderService {

  @InjectRepository(Order)
  private readonly orderRepo: Repository<Order>;

  // count(username: string, orderDate: string, status: OrderStatus, )

  async page(dto: OrderSearchDto) {

    let query: any = {};
    if (dto.username) {
      query.username = Like(`%${dto.username}%`);
    }

    if (dto.orderDate) {
      query.orderDate = Equal(dto.orderDate);
    }

    if (dto.status) {
      query.status = Equal(dto.status);
    }

    //Brackets
    const [list, count] = await this.orderRepo.createQueryBuilder().where(query)
      .skip((dto.pageNo-1) * dto.pageSize)
      .take(dto.pageSize)
      .getManyAndCount();

    return {
      list,
      pageNo: dto.pageNo,
      pageSize: dto.pageSize,
      total: count,
      offset: (dto.pageNo-1) * dto.pageSize,
      pages: Math.ceil(count / dto.pageSize),
    };
  }

  async create(dto: OrderCreateDto) {
    const order = this.orderRepo.create();
    order.username = dto.username;
    order.orderDate = dayjs(dto.orderDate).toDate();
    order.status = OrderStatus.Pending;
    return await this.orderRepo.save(order);
  }

  async update(id: string, dto: OrderUpdateDto) {
    const result = await this.orderRepo.createQueryBuilder()
      .update(Order)
      .set({ status: dto.status })
      .where({ id }).execute();

    console.log(result);
    return true;
  }

  async delete(id: string) {
    const result = await this.orderRepo.delete(id);
    console.log(result);
    return true;
  }

}
