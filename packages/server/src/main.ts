import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { loadConfig } from './common/utils/config.util';
import { TransformInterceptor } from './common/mvc/interceptors/transform.interceptor';
import { GlobalExceptionsFilter } from './common/mvc/exceptions/global.exception.filter';
import { HttpExceptionFilter } from './common/mvc/exceptions/http.exception.filter';
import { BizExceptionFilter } from './common/mvc/exceptions/biz.exception.filter';
import { ParseIntPipe, ValidationPipe } from '@nestjs/common';
import { makeThrow4Biz } from './common/shortcut';
import { ResultCode } from './common/constant/result-code';
import { BizException } from './common/mvc/exceptions/biz.exception';

async function bootstrap() {
  const { SYSTEM_CONFIG } = loadConfig();

  const app = await NestFactory.create(AppModule);

  // 统一响应体格式
  app.useGlobalInterceptors(new TransformInterceptor());

  // 异常过滤器
  app.useGlobalFilters(new GlobalExceptionsFilter(), new HttpExceptionFilter(), new BizExceptionFilter());

  // app.useGlobalPipes(new ValidationPipe({
  //   transform: true,
  //   exceptionFactory: (errors => {
  //     console.log('errors:', errors);
  //     makeThrow4Biz(ResultCode.PARAM_ERROR, errors.join(';'));
  //   }),
  // }));

  app.useGlobalPipes(new ValidationPipe({
    transform: true, // 启用参数类型自动转换，常规设置
    whitelist: true, // 监听参数白名单，常规设置
    // 禁止非白名单参数，存在非白名单属性报错。此项可根据需求而定，如果设置false，将剥离非白名单属性
    forbidNonWhitelisted: true,
    enableDebugMessages: true,
    stopAtFirstError: true,
    // 设置校验失败后的响应数据格式
    exceptionFactory: (errors) => {
      // 此处要注意，errors是一个对象数组，包含了当前所调接口里，所有验证失败的参数及错误信息。
      // 此处的处理是只返回第一个错误信息
      let msg = Object.values(errors[0].constraints)[0];
      return new BizException(ResultCode.PARAM_ERROR, msg);
    },
  }), );

  app.enableCors({
    origin: true,
    methods: 'GET,PUT,POST',
    allowedHeaders: 'Content-Type,Authorization',
    // exposedHeaders: 'Content-Range,X-Content-Range',
    credentials: true,
    maxAge: 3600,
  });


  // // 创建文档
  // if (!!SYSTEM_CONFIG.openapi && SYSTEM_CONFIG.openapi.enabled) {
  //   swaggerSetup(app, { ...SYSTEM_CONFIG.openapi.info });
  // }

  await app.listen(SYSTEM_CONFIG.port);

}

bootstrap().then();
