import {Module} from '@nestjs/common';
import {TypeOrmModule} from "@nestjs/typeorm";
import {SnakeNamingStrategy} from "typeorm-naming-strategies";
import {loadConfig} from './common/utils/config.util'
import {Order} from "./order/entities/order.entity";
import {ConfigModule} from "@nestjs/config";
import { InitDataService } from './order/services/init-data.service';
import { OrderService } from './order/services/order.service';
import { OrderController } from './order/controllers/order.controller';

const {MYSQL_CONFIG} = loadConfig()

@Module({
  imports: [
    ConfigModule.forRoot({
      ignoreEnvFile: true,
      isGlobal: true,
      load: [loadConfig]
    }),
    TypeOrmModule.forRoot({
      ...MYSQL_CONFIG,
      connectorPackage: "mysql2",
      namingStrategy: new SnakeNamingStrategy(),
      entities: [Order]
    }),
    TypeOrmModule.forFeature([Order]),
  ],
  controllers: [OrderController],
  providers: [InitDataService, OrderService]

})
export class AppModule {
}
