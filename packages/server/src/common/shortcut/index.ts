import { HttpException, HttpStatus } from '@nestjs/common';
import { BizException } from '../mvc/exceptions/biz.exception';
import { ResultCode } from '../constant/result-code';


export function makeThrow4Http(message: string, status: number = HttpStatus.INTERNAL_SERVER_ERROR) {
  return new HttpException(message, status);
}

export function makeThrow4Biz(code: ResultCode, message?: string) {
  return BizException.throw(code, message);
}

