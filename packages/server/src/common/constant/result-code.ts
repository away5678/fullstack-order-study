



export enum ResultCode {
  SUCCESS = 0,

  TOKEN_INVALID = 401001,
  REFRESH_TOKEN_ILLEGAL = 401002,
  UNKNOWN_ERROR = 500001,
  DAO_ERROR = 500100,
  PARAM_ERROR,
  XSS_ERROR,

  USER_LOGIN_ERROR = 600001,
}

export const ResultMessageMap: Map<ResultCode, string> = new Map([
  [ResultCode.SUCCESS, 'ok'],
  [ResultCode.UNKNOWN_ERROR, '未知错误'],
  [ResultCode.TOKEN_INVALID, '未登录或凭证已失效'],
  [ResultCode.REFRESH_TOKEN_ILLEGAL, '刷新凭证已失效'],
  [ResultCode.DAO_ERROR, '数据库查询错误'],
  [ResultCode.PARAM_ERROR, '参数错误'],
  [ResultCode.XSS_ERROR, '参数风险错误'],
  [ResultCode.USER_LOGIN_ERROR, '登录出错'],
]);
