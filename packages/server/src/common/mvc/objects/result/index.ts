import { ApiProperty } from '@nestjs/swagger';
import { PaginationOrderBy } from '../input';

/**
 * 返回数据的包装
 */
export class ResultWrapper<T> {

  @ApiProperty({ description: '时间' })
  timestamp: string;

  @ApiProperty({ description: '返回代码' })
  code: number;

  @ApiProperty({ description: '结果描述' })
  message: string;

  @ApiProperty({ description: '是否成功' })
  success: boolean;

  @ApiProperty()
  data?: T;

  @ApiProperty({ description: '请求地址' })
  path?: string;

  constructor(data: T, code: number = 0, message: string = 'ok') {
    this.data = data;
    this.code = code;
    this.message = message;
    this.timestamp = new Date().toISOString();
    this.success = (this.code === 0);
  }
}

export class PaginationOutputData<T> {
  @ApiProperty({ description: '当前页码' })
  pageNo: number;

  @ApiProperty({ description: '总页数' })
  pages: number;

  @ApiProperty({ description: '总行数' })
  total: number;

  @ApiProperty({ description: '每页行数' })
  pageSize: number;

  @ApiProperty({ description: '偏移行' })
  offset: number;

  @ApiProperty()
  list: T[];


}
