import { IsInt, IsOptional, Max, Min } from 'class-validator';
import { Transform } from 'class-transformer';

export abstract class PaginationInputData {

  @Min(1)
  @Max(1000)
  @IsInt()
  @Transform((params) => ('number' !== typeof params.value) ? parseInt(params.value) : params.value)
  @IsOptional()
  pageNo?: number = 1;

  @Min(1)
  @Max(200)
  @IsInt()
  @Transform((params) => ('number' !== typeof params.value) ? parseInt(params.value) : params.value)
  @IsOptional()
  pageSize?: number = 10;

  @IsOptional()
  orderbies?: PaginationOrderBy[] = [];
}

export enum OrderByDirection {
  DESC = 'DESC',
  ASC = 'ASC'
}

export class PaginationOrderBy {
  orderBy: string;
  orderByDirection: OrderByDirection;
}
