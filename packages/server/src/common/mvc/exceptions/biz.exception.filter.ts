import { ExceptionFilter, Catch, ArgumentsHost } from "@nestjs/common";
import { Response } from 'express';
import { BizException } from './biz.exception';

// Catch 的参数为 HttpException 将只捕获 HTTP 相关的异常错误
@Catch(BizException)
export class BizExceptionFilter implements ExceptionFilter {
  catch(exception: BizException, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse<Response>();
    const request = ctx.getRequest();
    const status = exception.getStatus();


    response.status(status).json({
      timestamp: new Date().toISOString(),
      code: exception.code,
      message: exception.message,
      success: false
    });
  }
}
