import { HttpException, HttpStatus } from '@nestjs/common';
import { ResultCode, ResultMessageMap } from '../../constant/result-code';

export class BizException extends HttpException {

  private readonly _code: ResultCode;

  constructor(code: ResultCode, message: string) {
    super(message, HttpStatus.OK);
    this._code = code;
  }

  public get code() {
    return this._code;
  }

  static throw(code: ResultCode, message?: string) {
    if (!message) {
      message = ResultMessageMap.get(code);
      if (!message) {
        message = '未知错误';
      }
    }
    throw new BizException(code, message);
  }
}
