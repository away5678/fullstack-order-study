import { ExceptionFilter, Catch, ArgumentsHost, HttpStatus, ServiceUnavailableException, } from '@nestjs/common';

// 捕获所有异常
@Catch()
export class GlobalExceptionsFilter implements ExceptionFilter {
  catch(exception: Error, host: ArgumentsHost) {
    const ctx = host.switchToHttp();
    const response = ctx.getResponse();
    const request = ctx.getRequest();

    // 非 HTTP 标准异常的处理。
    response.status(HttpStatus.SERVICE_UNAVAILABLE).json({
      timestamp: new Date().toISOString(),
      path: request.url,
      code: HttpStatus.SERVICE_UNAVAILABLE,
      message: new ServiceUnavailableException().getResponse(),
      success: false
    });
  }
}
