import { registerDecorator, ValidationOptions, ValidationArguments } from 'class-validator';
import dayjs from '../../../utils/dayjs.utils';

export const DefaultFormats = ['YYYY-MM-DD', 'YYYY/MM/DD'];

export function IsDateString(format?: string | string[], validationOptions?: ValidationOptions) {

  function defaultMessage(va: ValidationArguments): string {
    return '日期格式有误[$value]';
  }


  return function(object: Object, propertyName: string) {
    registerDecorator({
      name: 'isDateString',
      target: object.constructor,
      propertyName: propertyName,
      constraints: [format],
      options: {
        ...{ message: defaultMessage },
        ...validationOptions,
      },
      validator: {
        validate(value: any, args: ValidationArguments) {
          if (!format) {
            format = DefaultFormats;
          }
          return typeof value === 'string' && dayjs(value, format, true).isValid();
        },
      },
    });
  };
}
