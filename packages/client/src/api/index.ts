import { request } from './request.ts';

export enum OrderStatus {
  Pending = 'Pending',
  Processing = 'Processing',
  Completed = 'Completed',
}

export interface OrderSearchDto {
  username?: string;
  orderDate?: string;
  status?: string;
}

export interface OrderCreateDto {
  username: string;
  orderDate: string;
  status: OrderStatus;
}

export interface OrderUpdateDto {
  status: OrderStatus;
}

export function orderList(dto: OrderSearchDto) {
  return request({
    url: '/order/list',
    params: dto,
    method: 'get',
  });
}

export function orderCreate(dto: OrderCreateDto) {
  return request({
    url: '/order/create',
    data: dto,
    method: 'post',
  });
}

export function orderUpdate(id: string, dto: OrderUpdateDto) {
  return request({
    url: `/order/update/${id}`,
    data: dto,
    method: 'put',
  });
}

export function orderDelete(id: string) {
  return request({
    url: `/order/delete/${id}`,
    method: 'delete',
  });
}
