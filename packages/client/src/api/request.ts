import axios, { AxiosResponse } from 'axios';


export const request = axios.create({
  baseURL: 'http://localhost:4101', // 基础请求地址
  withCredentials: false, // 跨域请求是否需要携带 cookie
});

// 创建响应拦截
request.interceptors.response.use(
  (response: AxiosResponse) => {
    let { data, code, success, message } = response.data;
    // 处理自己的业务逻辑，比如判断 token 是否过期等等
    // 代码块
    return data;
  },
  (error: any) => {
    console.log(error.response);
    return Promise.reject(error);
  }
);
