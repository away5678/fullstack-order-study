import { OrderList } from './order-list.tsx';
import { useCallback, useEffect, useState } from 'react';
import { orderList, OrderStatus } from '../../api';
import { Page } from '../../components/page/page.tsx';

const StatusOptions = [
  { key: 'Pending', text: '待处理' },
  { key: 'Processing', text: '处理中' },
  { key: 'Completed', text: '已完成' },

];

export interface OrderRow {
  id: string;
  username: string;
  orderDate: string;
  status: OrderStatus;
}

export interface OrderPageDate {
  list: OrderRow[];
  pageNo: number;
  pageSize: number;
  pages: number;
  total: number;
  offset: number;
}


export interface OrderSearchForm {
  username?: string,
  orderDate?: string,
  status?: string,
  pageNo?: number,
  pageSize?: number,
}


export function OrderIndex() {

  const [formData, setFormData] = useState(() => {
    const _data: OrderSearchForm = {
      username: '', orderDate: '', status: '', pageNo: 1, pageSize: 10,
    };
    return _data;
  });

  const [pageDate, setPageDate] = useState(() => {
    const data: OrderPageDate = {
      list: [],
      pageNo: 1,
      pageSize: 10,
      pages: 0,
      total: 0,
      offset: 0,
    };
    return data;
  });

  const search = () => {
    console.log('doSearch: ', formData);
    orderList({ ...formData }).then((result: any) => {
      setPageDate({ ...result });
      // handlePageChange(result.pageNo, result.pageSize);
    });

  };

  useEffect(() => {
    search();
  }, [formData.pageNo, formData.pageSize]);

  const handleSearch = () => {
    setFormData({
      ...formData,
      pageNo: 1,
    });
    search();
  };

  const handleReset = useCallback(() => {
    setFormData({
      username: '', orderDate: '', status: '', pageNo: 1, pageSize: 10,
    });
  }, []);

  const handlePageChange = (pageNo: number, pageSize: number) => {
    if (formData.pageNo !== pageNo || formData.pageSize !== pageSize) {
      setFormData(() => {
        return {
          ...formData,
          pageNo,
          pageSize,
        };
      });
    }
  };

  return <div>
    <h3>订单列表</h3>

    <div style={{
      display: 'flex',
      flexDirection: 'row',
      alignItems: 'center',
      paddingBottom: '20px',
    }}>
      <div style={{ width: '120px', textAlign: 'right', paddingRight: '10px' }}>
        <label>用户名</label></div>
      <input value={formData.username} onInput={(e: { target: any }) => {
        setFormData({ ...formData, username: e.target.value });
      }} />
      <div style={{ width: '120px', textAlign: 'right', paddingRight: '10px' }}>
        <label>日期</label></div>
      <input value={formData.orderDate} onInput={(e: { target: any }) => {
        setFormData({ ...formData, orderDate: e.target.value });
      }} />
      <div style={{ width: '120px', textAlign: 'right', paddingRight: '10px' }}>
        <label>状态</label></div>
      <select value={formData.status} onChange={(e: { target: any }) => {
        setFormData({ ...formData, status: e.target.value });
      }}>
        {StatusOptions.map((item, index) => {
          return <option key={index} value={item.key}>{item.text}</option>;
        })}
      </select>

      <div style={{ paddingLeft: '10px', paddingRight: '10px' }}>
        <button onClick={handleSearch}>搜索</button>
        <button onClick={handleReset}>重置</button>
      </div>

    </div>

    <OrderList list={pageDate.list}
               page={<Page {...pageDate} updatePageData={handlePageChange} />} />

  </div>;

}




