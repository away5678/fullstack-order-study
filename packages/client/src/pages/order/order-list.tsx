import orderListCss from './order-list.module.css';
import { OrderRow } from './order-index.tsx';

const statusMap = {
  'Pending': {
    color: 'green',
    text: '待处理',
  },
  'Processing': {
    color: 'blue',
    text: '处理中',
  },
  'Completed': {
    color: 'gray',
    text: '已完成',
  },
};


export function OrderList({ list, page }: { list: OrderRow[], page: JSX.Element}) {

  const handleEdit = (id: string) => {
    console.log("Edit: ",id);
  };

  const handleDelete = (id: string) => {
    console.log("Delete: ",id);
  };



  return <table className={orderListCss.table}>
    <thead>
    <tr>
      <th style={{ width: '280px' }}>ID</th>
      <th style={{ width: '160px' }}>用户名</th>
      <th style={{ width: '160px' }}>订单日期</th>
      <th style={{ width: '160px' }}>状态</th>
      <th style={{ width: 'auto' }}>操作</th>
    </tr>
    </thead>
    <tbody>
    {list.map((item: OrderRow) => {
      return (
        <tr id={item.id} key={item.id}>
          <td>{item.id}</td>
          <td>{item.username}</td>
          <td>{item.orderDate}</td>
          <td style={{ color: statusMap[item.status].color }}>{statusMap[item.status].text}</td>
          <td style={{ textAlign: 'center' }}>
            <a style={{ cursor: 'pointer' }} onClick={handleEdit.bind(null, item.id)}>修改</a>
            &nbsp;
            &nbsp;
            <a style={{ cursor: 'pointer' }} onClick={handleDelete.bind(null, item.id)}>删除</a>
          </td>
        </tr>
      );

    })}
    </tbody>
    <tfoot>
    <tr>
      <td colSpan={5} style={{ backgroundColor: '#adadad' }}>
        <div style={{ textAlign: 'center' }}>
          {page}
          {/*<Page {...pageDate} updatePageNo={updatePageNo} />*/}
        </div>
      </td>
    </tr>
    </tfoot>

  </table>;
}

