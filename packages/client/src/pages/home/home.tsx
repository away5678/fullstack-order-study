import home from './home.module.css'
import { OrderIndex } from '../order/order-index.tsx';

export function Home() {
  return <div id="container" className={home.container}>
    <OrderIndex/>
  </div>;
}
