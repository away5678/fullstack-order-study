import ReactDOM from 'react-dom/client';
import './index.css';
import { Home } from './pages/home/home.tsx';

ReactDOM.createRoot(document.getElementById('root')!).render(
  <Home />,
);
