import { useEffect, useState } from 'react';
import pageCss from './page.module.css';


export function Page({ pageNo, pageSize, pages, total, offset, updatePageData }: {
  pageNo: number,
  pageSize: number,
  pages: number,
  total: number,
  offset: number,
  updatePageData: (pageNo: number, pageSize: number) => void,
}) {

  const [data, setDate] = useState(() => {
    return {
      isFirst: false,
      isLast: false,
      hasNext: false,
      hasPrev: false,
    };
  });

  useEffect(() => {

    // console.log(`Page useEffect: data:${[pageNo, pageSize, pages, total, offset]}`);

    const _data = {
      isFirst: false,
      isLast: false,
      hasNext: false,
      hasPrev: false,
    }

    if (pages > 0 && pageNo === 1) {
      _data.isFirst = true;
      _data.hasPrev = false;
    }

    if (pages > 0 && pageNo > 1 ) {
      _data.isFirst = false;
      _data.hasPrev = true;
    }

    if (pages > 0 && pageNo === pages) {
      _data.isLast = true;
      _data.hasNext = false;
    }

    if (pages > 0 && pageNo < pages) {
      _data.isLast = false;
      _data.hasNext = true;
    }

    setDate(_data)

    // console.log(`Page useEffect: data:${JSON.stringify(_data)}`);

  }, [pageNo, pageSize, pages, total, offset]);

  const handlePageSizeChange = (_pageSize: number) => {
    updatePageData(pageNo, _pageSize);
  };
  const handlePageNoChange = (_pageNo: number) => {
    updatePageData(_pageNo, pageSize);
  };

  return <div className={pageCss.page}>
    <button disabled={data.isFirst} onClick={handlePageNoChange.bind(null, 1)}> 首页</button>
    <button disabled={!data.hasPrev}
            onClick={handlePageNoChange.bind(null, pageNo - 1)}> 上一页
    </button>

    <div>当前第 {pageNo}页, 每页
      <select value={pageSize} onChange={(e: { target: any }) => {
        handlePageSizeChange(e.target.value);
      }}>
        <option value={10}>10</option>
        <option value={20}>20</option>
        <option value={30}>30</option>
        <option value={50}>50</option>
        <option value={100}>100</option>
      </select>
      条, 共{pages}页, {total}条数据
    </div>

    <button disabled={!data.hasNext}
            onClick={handlePageNoChange.bind(null, pageNo + 1)}> 下一页
    </button>
    <button disabled={data.isLast} onClick={handlePageNoChange.bind(null, pages)}> 尾页
    </button>
  </div>;
}
